
Pod::Spec.new do |s|

  s.name         = "Parse"
  s.version      = "1.7.5"
  s.summary      = "Parse framework."

  s.description  = <<-DESC
                   Parse framework.

                   DESC

  s.homepage     = "https://parse.com/apps/quickstart#parse_data/mobile/ios/native/new"

s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:project0-1/parse.git" }


  s.source_files  = "*.*", "Headers/*.{h,m,swift}", "Modules/*.modulemap"



end
